# SEBA Setup

Install all the components needed for SEBA

## Restart the software on the OLTs

To restart the software on the OLT, you need to ssh into it. You should find a `tmux` session running with two panels open running these processes:

```
cd /broadcom
./bal_core_dist -C :55001
```
```   
cd /broadcom
./openolt -C 127.0.0.1:40000 -A 127.0.0.1:50000
```
Stop and restart them

**ONF-POD OLT**
IP: ssh root@10.90.0.114 / onl

**SHAD OLT**
IP: ssh root@10.90.0.151 / onl

### Install a new version of the software on the OLT

Get the software version that you need and put it on the OLT.
Double-check that `bal_core_dist` or `openolt` processes are not running,
then `dpkg -i openolt.deb` (or the file you created)

Open `qax.soc` in the home directory, find the port section at the end and add this line
Downgrade the port: `port ce128 sp=40000`

## Configuring the POD

Refer to the subdirectories to configure different workflows

## Test the traffic

To test traffic ssh into `ssh ubuntu@10.90.0.51`, then enter one of the containers you can see with `lxc list`:

```
lxc exec qqclient /bin/bash
ping 192.168.0.254

lxc exec client2 /bin/bash
ping 192.168.1.254

lxc exec client3 /bin/bash
ping 192.168.1.254
```

The IPs listed here are the gateway ips, you can `ping 8.8.8.8` to test internet connectivity.

When a subscriber is in `status=enabled` it should be able to ping the internet.
When a subscriber is in `status=disabled` or `status=awaiting-validation` it should be able to ping the gateway.
When the ONU is disabled the subscriber should not be able to ping neither the gateway or the internet.
