# Configuring the R-CORD POD

> DISCLAIMER: This needs to be validated again

Push the recipes in this order:

`pod-configuration.yaml` contains the vRouter and Fabric setup
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @pod-configuration.yaml http://10.90.0.101:30007/run
```

`oss-service.yaml` adds _hippie-oss_ to the ServiceGraph (needed only for the bottom up provisioning)
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @oss-service.yaml http://10.90.0.101:30007/run
```

`subscriber.yaml` pre-provision the subscriber to use `c_tag=111` and also set `mac_address` and `ip_address`
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @subscriber.yaml http://10.90.0.101:30007/run
```

### Activate the OLT

`pod-olt.yaml` add the OLT to XOS, it also pre-provision the PONPort to have `s_tag=222` (remember to reset the OLT Software)
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @pod-olt.yaml http://10.90.0.101:30007/run
```

### Using also shad's OLT

Push `shad-olt.yaml` (adds a port to the fabric switch and push the OLTDevice)
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @shad-olt.yaml http://10.90.0.101:30007/run
```

### Configure data-plane connectivity for compute nodes over the fabric
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @fabric.yaml http://10.90.0.101:30007/run
```
