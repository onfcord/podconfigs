# ATT Workflow

This is the set of TOSCA required to configure the ONF POD for the At&t workflow.

## Deploy the components

To install:
```
 helm install -n nem-core nem-core/
 helm install -n nem-att profile/nem-att/
 helm install -n seba-substrate seba-substrate/
```

> At the moment some custom code is used for the demo, so we strongly suggest to use the `seba-values.yaml` in the root of this repo when installing the charts

To remove:
```
helm del --purge nem-att nem-core seba-substrate
```

### Configuring the POD

`pod-configuration.yaml` contains the vRouter, Fabric setup and HippieOSS Whitelist
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @pod-configuration.yaml http://10.90.0.101:30007/run
```

`subscriber.yaml` pre-provision the subscriber to use `c_tag=111` and `s_tag=222`
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @subscriber.yaml http://10.90.0.101:30007/run
```

Push the `sadis` app configuration:
```
curl -X POST --user karaf:karaf -H 'Content-Type:application/json' http://10.90.0.101:30120/onos/v1/network/configuration/apps/org.opencord.sadis --data @sadis.json
```

### Activate the OLT

`pod-olt.yaml` add the OLT to XOS, it also pre-provision the PONPort to have `s_tag=222` (remember to reset the OLT Software)
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @pod-olt.yaml http://10.90.0.101:30007/run
```

### Add shad's OLT

Pre-provision shad's OLT subscriber:
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @shad-subscribers.yaml http://10.90.0.101:30007/run
```

Push `shad-olt.yaml` (adds a port to the fabric switch and push the OLTDevice)
```
curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @shad-olt.yaml http://10.90.0.101:30007/run
```

## Authenticate the client

Run this command in the client
```
wpa_supplicant -i eth1 -Dwired -c /etc/wpa_supplicant/wpa_supplicant.conf
```

Send a dhcp request
```
```
