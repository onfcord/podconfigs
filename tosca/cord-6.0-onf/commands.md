# HELM INSTALLATIONS

helm install --name voltha-kafka \
--set replicas=1 \
--set persistence.enabled=false \
--set zookeeper.servers=1 \
--set zookeeper.persistence.enabled=false \
incubator/kafka

helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator
helm install --name cord-kafka \
--set replicas=1 \
--set persistence.enabled=false \
--set zookeeper.servers=1 \
--set zookeeper.persistence.enabled=false \
incubator/kafka

helm install -n voltha voltha

helm install -n onos-voltha -f configs/onos-voltha.yaml onos

helm install -n onos-fabric -f configs/onos-fabric.yaml onos

helm install -n xos-core xos-core

helm install -n rcord-lite xos-profiles/rcord-lite

helm install -n hippie-oss xos-services/hippie-oss

# ONCE EVERYTHING IS IN RUNNING STATE

curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @fabric.yaml http://10.90.0.101:30007/run

curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @oss-service.yaml http://10.90.0.101:30007/run

curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @subscriber.yaml http://10.90.0.101:30007/run

curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @pod-olt.yaml http://10.90.0.101:30007/run
