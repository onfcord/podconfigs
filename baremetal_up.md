# Going from blank nodes to SEBA demo

(2018-07-24, zdw): After breaking the nodes by rebooting them, and Matteo
wiping them with MaaS, here are the steps to go from a blank (well, Ubuntu
16.04 OS installed) nodes to a completed SEBA demo.

## Disabling swap

The ubuntu nodes come with 8GB of swap in a file in /swap.img .  K8s doesn't
like this.

```shell
sudo vi /etc/fstab
<delete swap line, save>
sudo swapoff /swap.img
sudo rm /swap.img
free
<should show 0 as swap size>
```

Rebooted first node to verify that this change stuck, and it did!

## Installing kubernetes

### Setting up SSH keys

I put my SSH key on these nodes using the
`automation-tools/kubespray-installer/copy-ssh-keys.sh` script.  (I specified
the SSH_PUBKEY_PATH because I have a multiple keys on my local system):

```shell
SSH_PUBKEY_PATH=~/.ssh/zdw.pub ./copy-ssh-keys.sh 10.90.0.101 10.90.0.102 10.90.0.103
```

Tested that passwordless remote login works by running:

```shell
ssh cord@10.90.0.101
```

No password was prompted for, works!

## Installing k8s w/kubespray

Followed instructions at: <https://guide.opencord.org/prereqs/k8s-multi-node.html>

```shell
./setup.sh -i onf 10.90.0.101 10.90.0.102 10.90.0.103
```

Died with:

```log
Generating The Inventory File
./setup.sh: line 44: python3: command not found
```

As I already had Python 3 installed via MacPorts, just not named in the way the
script wanted, fixed with:

```shell
sudo port select --set python3 python36
```

Reran setup.sh ... died with:

```log
Installing Kubespray
ERROR! no action detected in task. This often indicates a misspelled module name, or incorrect module path.

The error appears to have been in 'cordrepo/automation-tools/kubespray-installer/kubespray/roles/vault/handlers/main.yml': line 44, column 3, but may
be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:


- name: unseal vault
  ^ here
```

Needs `ansible-modules-hashivault` installed...

```shell
virtualenv ks_inst
source ks_inst/bin/activate
pip install -r kubespray/requirements.txt
```

Reran setup.sh ... died with:

```ansible
TASK [kubernetes/preinstall : Pre-upgrade | check if old credential dir exists] *****************************************************************
fatal: [node1 -> localhost]: FAILED! => {"changed": false, "module_stderr": "sudo: a password is required\n", "module_stdout": "", "msg": "MODULE FAILURE", "rc": 1}

NO MORE HOSTS LEFT ******************************************************************************************************************************
```

Determined to be a coding error in kubespray regarding `local_action` (similar
issue: <https://github.com/ansible/ansible/issues/18770> ). Fix:

```patch
diff --git a/roles/kubernetes/preinstall/tasks/pre_upgrade.yml b/roles/kubernetes/preinstall/tasks/pre_upgrade.yml
index 63cbc9be..94508e73 100644
--- a/roles/kubernetes/preinstall/tasks/pre_upgrade.yml
+++ b/roles/kubernetes/preinstall/tasks/pre_upgrade.yml
@@ -5,6 +5,7 @@
     path: "{{ inventory_dir }}/../credentials"
   vars:
     ansible_python_interpreter: "/usr/bin/env python"
+    ansible_become: false
   register: old_credential_dir
   become: no

@@ -14,6 +15,7 @@
     path: "{{ inventory_dir }}/credentials"
   vars:
     ansible_python_interpreter: "/usr/bin/env python"
+    ansible_become: false
   register: new_credential_dir
   become: no
   when: old_credential_dir.stat.exists
@@ -24,5 +26,6 @@
     creates: "{{ inventory_dir }}/credentials"
   vars:
     ansible_python_interpreter: "/usr/bin/env python"
+    ansible_become: false
   become: no
   when: old_credential_dir.stat.exists and not new_credential_dir.stat.exists
```

Reran setup.sh ... died with:

```log
RUNNING HANDLER [docker : Docker | pause while Docker restarts] *********************************************************************************
Pausing for 10 seconds
(ctrl+C then 'C' = continue early, ctrl+C then 'A' = abort)
[docker : Docker | pause while Docker restarts]
Waiting for docker restart:
An exception occurred during task execution. To see the full traceback, use -vvv. The error was: error: (25, 'Inappropriate ioctl for device')
fatal: [node1]: FAILED! => {"msg": "Unexpected failure during module execution.", "stdout": ""}

RUNNING HANDLER [docker : Docker | wait for docker] *********************************************************************************************
changed: [node3] => {"attempts": 1, "changed": true, "cmd": ["/usr/bin/docker", "images"], "delta": "0:00:00.037224", "end": "2018-07-25 01:13:09.996489", "rc": 0, "start": "2018-07-25 01:13:09.959265", "stderr": "", "stderr_lines": [], "stdout": "REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE", "stdout_lines": ["REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE"]}
changed: [node2] => {"attempts": 1, "changed": true, "cmd": ["/usr/bin/docker", "images"], "delta": "0:00:00.033341", "end": "2018-07-25 01:13:10.088747", "rc": 0, "start": "2018-07-25 01:13:10.055406", "stderr": "", "stderr_lines": [], "stdout": "REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE", "stdout_lines": ["REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE"]}
```

Ansible is broken: <https://github.com/ansible/ansible/issues/41717>

To record logs while I'm doing this, my invocation of ansible is piped into
tee: `| tee <logfile>` and that breaks the `pause` module... argh.

Fix:

```shell
pip uninstall ansible
pip install ansible==2.5.3
```

Reran setup.sh ... finally worked!

```shell
$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                    READY     STATUS    RESTARTS   AGE
kube-system   calico-node-2mqjw                       1/1       Running   0          2m
kube-system   calico-node-c6vxv                       1/1       Running   0          2m
kube-system   calico-node-v4dhl                       1/1       Running   0          2m
kube-system   kube-apiserver-node1                    1/1       Running   0          3m
kube-system   kube-apiserver-node2                    1/1       Running   0          3m
kube-system   kube-controller-manager-node1           1/1       Running   0          4m
kube-system   kube-controller-manager-node2           1/1       Running   0          4m
kube-system   kube-dns-7bd4d5fbb6-7wfdh               3/3       Running   0          1m
kube-system   kube-dns-7bd4d5fbb6-gtt54               3/3       Running   0          1m
kube-system   kube-proxy-node1                        1/1       Running   0          3m
kube-system   kube-proxy-node2                        1/1       Running   0          3m
kube-system   kube-proxy-node3                        1/1       Running   0          4m
kube-system   kube-scheduler-node1                    1/1       Running   0          4m
kube-system   kube-scheduler-node2                    1/1       Running   0          4m
kube-system   kubedns-autoscaler-679b8b455-qx8bw      1/1       Running   0          1m
kube-system   kubernetes-dashboard-55fdfd74b4-qldtd   1/1       Running   0          1m
kube-system   local-volume-provisioner-dpj24          1/1       Running   0          2m
kube-system   local-volume-provisioner-l6v4g          1/1       Running   0          2m
kube-system   local-volume-provisioner-rkwmv          1/1       Running   0          2m
kube-system   nginx-proxy-node3                       1/1       Running   0          4m
kube-system   tiller-deploy-5c688d5f9b-fmlzr          1/1       Running   0          1m

$ helm version
Client: &version.Version{SemVer:"v2.9.1", GitCommit:"20adb27c7c5868466912eebdf6664e7390ebe710", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.9.1", GitCommit:"20adb27c7c5868466912eebdf6664e7390ebe710", GitTreeState:"clean"}
```

## Installing insecure docker registry

Follow instructions here <https://guide.opencord.org/prereqs/docker-registry.html>

After install:

```shell
$ curl -X GET http://10.90.0.101:30500/v2/_catalog
{"repositories":[]}
```

Works!

## Pushing images to docker registry

Skipped this as we can probably use our own, published images...

## Loading SEBA charts

Checked out seba manifest, and started following README.md

```
cd seba/helm-charts
helm repo add cord https://charts.opencord.org/master
helm dep update nem-core
helm dep update profile/bng-in-fabric
Error: Can't get a valid version for repositories onos-service, volt. Try changing the version constraint in requirements.yaml`
```

Probably due to: <https://gerrit.opencord.org/c/10229/>, patch to resolve this:

```patch
diff --git a/helm-charts/profile/bng-in-fabric/requirements.yaml b/helm-charts/profile/bng-in-fabric/requirements.yaml
index d4c3a95..9ff7ace 100644
--- a/helm-charts/profile/bng-in-fabric/requirements.yaml
+++ b/helm-charts/profile/bng-in-fabric/requirements.yaml
@@ -18,13 +18,13 @@ dependencies:
   version: 1.0.0
   repository: https://charts.opencord.org/master/
 - name: onos-service
-  version: 2.0.0
+  version: 2.0.1
   repository: https://charts.opencord.org/master/
 - name: fabric
   version: 2.0.0
   repository: https://charts.opencord.org/master/
 - name: volt
-  version: 2.0.0
+  version: 2.0.1
   repository: https://charts.opencord.org/master/
 - name: vsg-hw
   version: 1.0.0
```

After this...

```
helm dep update profile/bng-in-fabric
helm dep update seba-substrate
helm install nem-core -n nem-core
helm install profile/bng-in-fabric -n nem-svc
```

`nem-core`, all containers are in `RUNNING` state. `nem-svc` not so lucky:

```shell
$ helm status nem-svc
==> v1/Pod(related)
NAME                                      READY  STATUS            RESTARTS  AGE
nem-svc-fabric-ccc6f879-mhdk8             1/1    Running           0         5m
nem-svc-hippie-oss-6f578bf788-mz2qg       0/1    CrashLoopBackOff  5         5m
nem-svc-onos-service-77d567d57-qlhjp      1/1    Running           0         5m
nem-svc-rcord-54dd4cf787-cgvlb            1/1    Running           0         5m
nem-svc-volt-597bc7b6c8-l4vmn             1/1    Running           0         5m
nem-svc-vrouter-5884b8689f-gqwnx          1/1    Running           0         5m
nem-svc-vsg-hw-655598bb6d-sh49w           1/1    Running           0         5m
nem-svc-bng-in-fabric-tosca-loader-nz856  0/1    CrashLoopBackOff  5         5m
```

Looking into the issues:

```shell
 $ kubectl logs nem-svc-hippie-oss-6f578bf788-mz2qg
Starting                       handlers={'console': {'class': 'logging.StreamHandler'}, 'file': {'maxBytes': 10485760, 'backupCount': 5, 'class': 'logging.handlers.RotatingFileHandler', 'filename': '/var/log/xos.log'}} level_override=None loggers={'multistructlog': {'level': 'DEBUG', 'handlers': ['console', 'file']}} version=1
Connecting to the gRPC API
Traceback (most recent call last):
  File "/opt/xos/synchronizers/hippie-oss/hippie-oss-synchronizer.py", line 31, in <module>
    mod = importlib.import_module("xos-synchronizer")
  File "/usr/lib/python2.7/importlib/__init__.py", line 37, in import_module
    __import__(name)
  File "/opt/xos/synchronizers/hippie-oss/../../synchronizers/new_base/xos-synchronizer.py", line 26, in <module>
    from synchronizers.new_base.modelaccessor import *
  File "/opt/xos/synchronizers/new_base/modelaccessor.py", line 300, in <module>
    config_accessor()
  File "/opt/xos/synchronizers/new_base/modelaccessor.py", line 291, in config_accessor
    config_accessor_grpcapi()
  File "/opt/xos/synchronizers/new_base/modelaccessor.py", line 246, in config_accessor_grpcapi
    raise Exception("%s does not exist" % fn)
Exception: /opt/xos/services/hippie-oss/credentials/xosadmin@opencord.org does not exist
```

Seems to be fixed here?: <https://gerrit.opencord.org/c/10341/> and
<https://gerrit.opencord.org/c/10342/>

```
$ kubectl logs nem-svc-hippie-oss-6f578bf788-mz2qg
...
Loading: /opt/tosca/060-onos-service-fabric.yaml
Created models: ['service#ONOS_Fabric', 'onos_app#segmentrouting', 'onos_app#vrouter', 'onos_app#openflow', 'onos_app#netcfghostprovider']
Loading: /opt/tosca/061-onos-voltha-service-fabric.yaml

The input "/opt/xos-tosca/src/tosca//tmp.yaml" failed validation with the following error(s):

	KeyError: 'Node template "service#ONOS_CORD" was not found in "onos_app#olt".'
		File src/main.py, line 64, in <module>
			Main().start()
		File src/main.py, line 60, in start
			TOSCA_WebServer()
		File /opt/xos-tosca/src/web_server/main.py, line 104, in __init__
			self.app.run('0.0.0.0', '9102')
		File /usr/local/lib/python2.7/dist-packages/klein/app.py, line 350, in run
			reactor.run()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/base.py, line 1199, in run
			self.mainLoop()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/base.py, line 1208, in mainLoop
			self.runUntilCurrent()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/base.py, line 828, in runUntilCurrent
			call.func(*call.args, **call.kw)
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/defer.py, line 457, in callback
			self._startRunCallbacks(result)
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/defer.py, line 565, in _startRunCallbacks
			self._runCallbacks()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/defer.py, line 651, in _runCallbacks
			current.result = callback(current.result, *args, **kw)
		File /opt/xos-tosca/src/web_server/main.py, line 43, in execute_tosca
			self.parser.execute()
		File /opt/xos-tosca/src/tosca/parser.py, line 196, in execute
			self.template = ToscaTemplate(self.recipe_file)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tosca_template.py, line 101, in __init__
			self.topology_template = self._topology_template()
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tosca_template.py, line 120, in _topology_template
			None)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/topology_template.py, line 60, in __init__
			self.graph = ToscaGraph(self.nodetemplates)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tpl_relationship_graph.py, line 19, in __init__
			self._create()
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tpl_relationship_graph.py, line 40, in _create
			relation = node.relationships
		File /usr/local/lib/python2.7/dist-packages/toscaparser/nodetemplate.py, line 61, in relationships
			explicit = self._get_explicit_relationship(r, value)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/nodetemplate.py, line 94, in _get_explicit_relationship
			% {'node': node, 'name': self.name}))

	KeyError: 'Node template "service#ONOS_CORD" was not found in "onos_app#dhcpl2relay".'
		File src/main.py, line 64, in <module>
			Main().start()
		File src/main.py, line 60, in start
			TOSCA_WebServer()
		File /opt/xos-tosca/src/web_server/main.py, line 104, in __init__
			self.app.run('0.0.0.0', '9102')
		File /usr/local/lib/python2.7/dist-packages/klein/app.py, line 350, in run
			reactor.run()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/base.py, line 1199, in run
			self.mainLoop()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/base.py, line 1208, in mainLoop
			self.runUntilCurrent()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/base.py, line 828, in runUntilCurrent
			call.func(*call.args, **call.kw)
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/defer.py, line 457, in callback
			self._startRunCallbacks(result)
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/defer.py, line 565, in _startRunCallbacks
			self._runCallbacks()
		File /usr/local/lib/python2.7/dist-packages/twisted/internet/defer.py, line 651, in _runCallbacks
			current.result = callback(current.result, *args, **kw)
		File /opt/xos-tosca/src/web_server/main.py, line 43, in execute_tosca
			self.parser.execute()

http: warning: HTTP 500 Internal Server Error
		File /opt/xos-tosca/src/tosca/parser.py, line 196, in execute
			self.template = ToscaTemplate(self.recipe_file)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tosca_template.py, line 101, in __init__
			self.topology_template = self._topology_template()
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tosca_template.py, line 120, in _topology_template
			None)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/topology_template.py, line 60, in __init__
			self.graph = ToscaGraph(self.nodetemplates)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tpl_relationship_graph.py, line 19, in __init__
			self._create()
		File /usr/local/lib/python2.7/dist-packages/toscaparser/tpl_relationship_graph.py, line 40, in _create
			relation = node.relationships
		File /usr/local/lib/python2.7/dist-packages/toscaparser/nodetemplate.py, line 61, in relationships
			explicit = self._get_explicit_relationship(r, value)
		File /usr/local/lib/python2.7/dist-packages/toscaparser/nodetemplate.py, line 94, in _get_explicit_relationship
			% {'node': node, 'name': self.name}))
```

Seems to need ONOS_CORD loaded? Trying to load it via `seba-substrate`:

```
$ helm install seba-substrate -n seba-substrate
$ helm upgrade seba-substrate --set voltha.etcd-operator.customResources.createEtcdClusterCRD=true seba-substrate
Error: UPGRADE FAILED: apiVersion "etcd.database.coreos.com/v1beta2" in seba-substrate/charts/voltha/charts/etcd-operator/templates/etcd-cluster-crd.yaml is not available
```

VOLTHA instructions don't give a context for where you should be in the
filesystem before running `helm dep build voltha` (should that be `update` ?):
<https://guide.opencord.org/charts/voltha.html>

Stopped work here, resumed after a period of time.

State of system:

```shell
$ helm status seba-substrate
...
voltha-56fddc76fb-g8lhb                                          0/1    CrashLoopBackOff  132       11h
...
$ helm status nem-svc
...
nem-svc-hippie-oss-6f578bf788-mz2qg       0/1    CrashLoopBackOff  144       11h
nem-svc-bng-in-fabric-tosca-loader-nz856  0/1    CrashLoopBackOff  142       11h
```

Reran:

```
helm upgrade seba-substrate --set voltha.etcd-operator.customResources.createEtcdClusterCRD=true seba-substrate
```

It completed properly, and seemed to fix voltha container

```
voltha-56fddc76fb-g8lhb                                          1/1    Running  133       11h
```

Stopped here, stuck on `hippie-oss` issue, and `Node template "service#ONOS_CORD" was not found ...` issue.

Matteo applied this patchset: <https://gerrit.opencord.org/c/10371/> and Andy will apply this patchset (does the patch above to `bng-in-fabric`), but that might not include hippie-oss v1.0.1, which is published in the `cord-6.0` branch, so:

```diff
$ git diff profile/bng-in-fabric/requirements.yaml
diff --git a/helm-charts/profile/bng-in-fabric/requirements.yaml b/helm-charts/profile/bng-in-fabric/requirements.yaml
index d4c3a95..9471510 100644
--- a/helm-charts/profile/bng-in-fabric/requirements.yaml
+++ b/helm-charts/profile/bng-in-fabric/requirements.yaml
@@ -18,13 +18,13 @@ dependencies:
   version: 1.0.0
   repository: https://charts.opencord.org/master/
 - name: onos-service
-  version: 2.0.0
+  version: 2.0.1
   repository: https://charts.opencord.org/master/
 - name: fabric
   version: 2.0.0
   repository: https://charts.opencord.org/master/
 - name: volt
-  version: 2.0.0
+  version: 2.0.1
   repository: https://charts.opencord.org/master/
 - name: vsg-hw
   version: 1.0.0
@@ -33,5 +33,5 @@ dependencies:
   version: 2.0.0
   repository: https://charts.opencord.org/master/
 - name: hippie-oss
-  version: 1.0.0
-  repository: https://charts.opencord.org/master/
+  version: 1.0.1
+  repository: https://charts.opencord.org/cord-6.0/
```

then load it:

```shell
helm repo add cord6 https://charts.opencord.org/cord-6.0
helm dep up profile/bng-in-fabric
helm install profile/bng-in-fabric -n nem-svc
```

Working, Checked by going to <http://10.90.0.101:30001/> and logging in.

# onf_podconfigs README.md instructions

```
$ ssh root@10.90.0.114
The authenticity of host '10.90.0.114 (10.90.0.114)' can't be established.
ECDSA key fingerprint is SHA256:dvOPYgxH0tBC4w7ylucQORFXtZQEErh5NBhRcMyQpB0.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.90.0.114' (ECDSA) to the list of known hosts.
root@10.90.0.114's password:
Last login: Tue Jul 24 16:14:25 2018 from 10.3.1.3
root@localhost:~# cd /broadcom/
root@localhost:/broadcom# ls
asfx816bbx.hex	    bcm.user	      linux-kernel-bde.ko  rc.soc
bal_cli		    combo28_dram.soc  linux-user-bde.ko    reload.soc
bal_config.ini	    config.bcm	      onu_mgmt_config.ini
bal_core_dist	    dnx.soc	      openolt
bcm88470_board.soc  dune.soc	      qax.soc
root@localhost:/broadcom# ./bal_core_dist -C 127.0.0.1:40000 -A 127.0.0.1:50000
*** Invalid parameter: -A
Usage: bal_core_dist arguments
-C                        [core_mgmt_ip:]port  [IP address:]UDP port where the core listens for messages from the BAL Public API
-L                                      level  CLI level: guest | admin | debug
-ne                                            Disable line editing
-f                           script_file_name  Script containing BAL CLI commands
-F                              log_file_name  Log into file
    -syslog                                    Log to syslog
-nl                                            Disable logger
-h ,--help                                     This help
```

Commands seem to differ - from the command history:

`./bal_core_dist -C :55001`

and:

`./openolt -C 127.0.0.1:55001`


Waiting on this for now, moved onto TOSCA loading.

# Loading SEBA TOSCA

```shell
$ cd tosca

$ curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @pod-configuration.yaml http://10.90.0.101:30007/run
Created models: ['switch#leaf_1', 'port#olt_port', 'interface#olt_interface', 'port#vrouter_port', 'interface#vrouter_interface', 'vrouter#my_vrouter', 'route#my_route']

$ curl -H "xos-username: admin@opencord.org" -H "xos-password: letmein" -X POST --data-binary @oss-service.yaml http://10.90.0.101:30007/run
	UnknownFieldError: "properties" of template "service#oss" contains unknown field "create_on_discovery". Refer to the definition to verify valid values.
	UnknownFieldError: "properties" of template "service#oss" contains unknown field "whitelist". Refer to the definition to verify valid values.
```

Looked for correct values in http://10.90.0.101:30006/apidocs

Problem with 1.0.1 version of `hippie-oss` not being compatible with this
tosca. Switched to `1.1.0-dev` version from this patch:
https://gerrit.opencord.org/#/c/10377/

```shell
diff --git a/helm-charts/profile/bng-in-fabric/requirements.yaml b/helm-charts/profile/bng-in-fabric/requirements.yaml
index 9ff7ace..4a08adf 100644
--- a/helm-charts/profile/bng-in-fabric/requirements.yaml
+++ b/helm-charts/profile/bng-in-fabric/requirements.yaml
@@ -33,5 +33,5 @@ dependencies:
   version: 2.0.0
   repository: https://charts.opencord.org/master/
 - name: hippie-oss
-  version: 1.0.0
+  version: 1.1.0-dev
   repository: https://charts.opencord.org/master/
```

Wiped out pod and reloaded:

```
$ helm delete --purge nem-core nem-svc seba-substrate
$ helm install nem-core -n nem-core
$ helm install profile/bng-in-fabric -n nem-svc
$ helm install seba-substrate -n seba-substrate
<wait for seba-substrate to install>
$ helm upgrade seba-substrate --set voltha.etcd-operator.customResources.createEtcdClusterCRD=true seba-substrate
```

Ran out of time at this point.

